### install dhcp server

  - install packages
  > $ sudo dnf install dhcp-server -y
  >

  - start commands
  > sudo firewall-cmd --permanent --add-service=dhcp
  >
  
  > sudo firewall-cmd --reload
  >

  > sudo systemctl enable --now dhcpd -q
  >

  > sudo systemctl restart dhcpd -q
  >

### install pxe

  - install packages
  > $ sudo dnf install syslinux tftp-server -y
  >

  - install TFTP Server with SYSLINUX Bootloaders
  > sudo dnf install syslinux tftp-server -y
  >

  > $ sudo cp -r /usr/share/syslinux/* /var/lib/tftpboot
  >

  - configuration PXE Server File
  > $ sudo mkdir -p /var/lib/tftpboot/pxelinux.cfg
  >

  > $ sudo chmod -R 0777 /var/lib/tftpboot/pxelinux.cfg
  >

  > $ sudo chcon -t tftpdir_rw_t  /var/lib/tftpboot/*
  >

  > $ sudo restorecon -FR /var/lib/tftpboot
  >

  > $ sudo touch /var/lib/tftpboot/pxelinux.cfg/default
  >

  - now edit PXE configuration file with RHEL installation option. Note that all paths used in this file must be placed in /var/lib/tftpboot directory

        $ sudo cat /var/lib/tftpboot/pxelinux.cfg/default
        
        default menu.c32
        prompt 0
        timeout 300
        ONTIMEOUT local

        label 1
          menu label ^1) Install RHEL 8 with Local Repo
          kernel rhel8/vmlinuz
          append initrd=rhel8/initrd.img method=ftp://192.168.130.152/pub devfs=nomount

        label rescue
          menu label ^Rescue installed system
          kernel vmlinuz
          append initrd=initrd.img rescue

### 
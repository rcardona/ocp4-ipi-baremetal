### Here is an example of the install-config.yaml file

    apiVersion: v1
    baseDomain: <domain>
    metadata:
      name: <cluster_name>
    networking:
      machineNetwork:
      - cidr: <public_cidr>
      networkType: OVNKubernetes
    compute:
    - name: worker
      replicas: 2 
    controlPlane:
      name: master
      replicas: 3
      platform:
        baremetal: {}
    platform:
      baremetal:
        apiVIPs:
          - <api_ip>
        ingressVIPs:
          - <wildcard_ip>
        bootstrapExternalStaticIP: <bootstrap_static_ip_address> 
        bootstrapExternalStaticGateway: <bootstrap_static_gateway> 
        hosts:
          - name: openshift-master-0
            role: master
            bmc:
              address: idrac-virtualmedia://<out-of-band-ip>/redfish/v1/Systems/System.Embedded.1
              username: <user>
              password: <password>
            bootMACAddress: <NIC1_mac_address>
            rootDeviceHints:
             deviceName: "/dev/disk/by-id/<disk_id>" 
          - name: openshift_master_
            role: master
            bmc:
              address: idrac-virtualmedia://<out-of-band-ip>/redfish/v1/Systems/System.Embedded.1
              username: <user>
              password: <password>
            bootMACAddress: <NIC1_mac_address>
            rootDeviceHints:
             deviceName: "/dev/disk/by-id/<disk_id>" 
          - name: openshift_master_2
            role: master
            bmc:
              address: idrac-virtualmedia://<out-of-band-ip>/redfish/v1/Systems/System.Embedded.1
              username: <user>
              password: <password>
            bootMACAddress: <NIC1_mac_address>
            rootDeviceHints:
             deviceName: "/dev/disk/by-id/<disk_id>" 
          - name: openshift_worker_0
            role: worker
            bmc:
              address: idrac-virtualmedia://<out-of-band-ip>/redfish/v1/Systems/System.Embedded.1
              username: <user>
              password: <password>
            bootMACAddress: <NIC1_mac_address>
          - name: openshift_worker_1
            role: worker
            bmc:
              address: idrac-virtualmedia://<out-of-band-ip>/redfish/v1/Systems/System.Embedded.1
              username: <user>
              password: <password>
            bootMACAddress: <NIC1_mac_address>
            rootDeviceHints:
             deviceName: "/dev/disk/by-id/<disk_id>" 
    pullSecret: '<pull_secret>'
    sshKey: '<ssh_pub_key>'
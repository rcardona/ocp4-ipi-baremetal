ESTE ES MY DHCP



- example of dhcp entries

      $ sudo cat /etc/dhcp/dhcpd.conf

      option space pxelinux;
      option pxelinux.magic code 208 = string;
      option pxelinux.configfile code 209 = text;
      option pxelinux.pathprefix code 210 = text;
      option pxelinux.reboottime code 211 = unsigned integer 32;
      option architecture-type code 93 = unsigned integer 16;

      subnet 10.10.0.0 netmask 255.255.255.0 {
        option routers 10.10.0.1;
        option domain-name-servers 10.10.0.1;
        next-server 10.10.0.1;
        filename "pxelinux.0";

        host bootstrap { hardware ethernet 54:52:00:01:02:03; fixed-address 10.10.0.9; }
        host master-0 { hardware ethernet 54:52:00:01:02:04; fixed-address 10.10.0.10; }
        host master-1 { hardware ethernet 54:52:00:01:02:05; fixed-address 10.10.0.11; }
        host master-2 { hardware ethernet 54:52:00:01:02:06; fixed-address 10.10.0.12; }
        host worker-0 { hardware ethernet 54:52:00:01:02:07; fixed-address 10.10.0.20; }
        host worker-1 { hardware ethernet 54:52:00:01:02:08; fixed-address 10.10.0.21; }
        host worker-2 { hardware ethernet 54:52:00:01:02:09; fixed-address 10.10.0.22; }
      }
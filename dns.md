# DNS configutaion

This is an example where the cluster name is **ocp4**, and the dns subdomain assigned to the cluster is **poc.company.com**. And where the floating virtual IPs assigned for the api server and ingress router endpoint are as follows.

__cluster name:__ ocp4

__dns domain:__ poc.company.com

__api server VIp:__ 10.10.10.5

__ingress router VIp:__ 10.10.10.6

- A records

      api.ocp4.poc.company.com     10.10.0.7
      *.apps.ocp4.poc.company.com  10.10.0.8

# Opensihift v4 Installer Provisioned Infrastructure

OpenShift Enterprise Cluster offers great flexibility across various installation configurations. During this installation process, it utilizes the Installer Provisioned Infrastructure binary, which enables the installation package to provision the foundational infrastructure before deploying the OCP Enterprise components.

The objective is to deploy a standard OCP enterprise as follows

- [Red Hat Cloud](https://console.redhat.com/openshift/)

- [Assisted Installer](https://docs.openshift.com/container-platform/4.13/installing/installing_on_prem_assisted/installing-on-prem-assisted.html)


![Diagram](media/ocp-cluster.png)

## Use Cases

- [x] Automated deployment with IPI with Assisted Installer, [DOCS](https://docs.openshift.com/container-platform/4.13/installing/installing_with_agent_based_installer/preparing-to-install-with-agent-based-installer.html)

- [x] Cofiguring Identity Provider [DOCS](https://docs.openshift.com/container-platform/4.13/authentication/identity_providers/configuring-basic-authentication-identity-provider.html)

- [x] Install and configure Openshift Data Foudnation [DOCS](https://access.redhat.com/documentation/en-us/red_hat_openshift_data_foundation/4.12/html-single/deploying_openshift_data_foundation_using_bare_metal_infrastructure/index)

- [x] Monitoring and Logging [DOCS](https://docs.openshift.com/container-platform/4.12/monitoring/monitoring-overview.html)
 
- [x] Enabling OpenShift API for Data Protection (Velero) [DOCS](https://access.redhat.com/documentation/en-us/openshift_container_platform/4.12/html/backup_and_restore/application-backup-and-restore), here an [EXAMPLE](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/data-protection/configure-data-protection.md)

- [x] Install and configure Openshift Virtualization, [DOCS](https://docs.openshift.com/container-platform/4.13/virt/about-virt.html)

- [x] RBAC

- [ ] VM migration from vSphere to Openshift Virtualization

- [x] Automatic VM Failover on Openshift Virtualization

- [x] Enabling Secondary Interfaces Hosted VMs (NetworkAttachmentDefinition)

- [ ] Configuring Network Policies to control outbound application access [DOCS](https://gitlab.com/rcardona/ocp4-tasks/-/blob/master/networking/network-policies/egress.md), [EXAMPLE APP](https://gitlab.com/rcardona/ocp4-apps-examples/-/blob/master/nginx/nginx-postgres.md)

## Prerequisites


- [x] A baremetal provisioner node with Red Hat Enterprise Linux (v8.x or newer) installed
- [x] (3x) control plane nodes, and (at least 2x) compute nodes. This is particularly necessary because OCP Virtualization will be installed as well
- [x] Baseboard management controller (BMC) access to each node
- [x] A routable network, which will serve as installation, management and operational network
- [x] OCP nodes access to the internet
- [x] DNS service with corresponding dns records, [example](dns.md)

  [HERE](https://docs.openshift.com/container-platform/4.13/installing/installing_bare_metal_ipi/ipi-install-prerequisites.html) the official documentation


## Steps

### 0 - Preparing the provisioner node

---

#### 0.1 - Create a non-root user (kni) and provide that user with sudo privileges

> $ sudo useradd kni
>

> $ sudo passwd kni
>

> $ sudo echo "kni ALL=(root) NOPASSWD:ALL" | tee -a /etc/sudoers.d/kni
>

> $ sudo su - kni -c "ssh-keygen -t ed25519 -f /home/kni/.ssh/id_rsa -N ''"
>

#### 0.2 - Enabling subscriptions

> $ su - kni
>

> $ sudo subscription-manager register --username=<user> --password=<pass> --auto-attach
>

> $ sudo subscription-manager repos --enable=rhel-8-for-<architecture>-appstream-rpms --enable=rhel-8-for-<architecture>-baseos-rpms
>

#### 0.3 - Installating packages

> $ sudo dnf install -y libvirt qemu-kvm mkisofs python3-devel jq ipmitool
>

#### 0.4 - Configuring *libvirt*

> $ sudo usermod --append --groups libvirt <user>
>

> $ sudo systemctl start firewalld
>

> $ sudo firewall-cmd --zone=public --add-service=http --permanent
>

> $ sudo sudo firewall-cmd --reload
>

> $ sudo systemctl enable libvirtd --now
>

> $ sudo virsh pool-define-as --name default --type dir --target /var/lib/libvirt/images
>

> $ sudo virsh pool-start default
>

> $ sudo virsh pool-autostart default
>

#### 0.5 - Create a pull-secret.txt

Visit this [SITE](https://console.redhat.com/openshift/install/pull-secret) to het the secret

> $ vim pull-secret.txt
>

---

### 1 - Configuring networking

---

#### 1.0 - Setting interface

> $ export PUB_CONN=<baremetal_nic_name>
>

    $ sudo nohup bash -c "
        nmcli con down \"$PUB_CONN\"
        nmcli con delete \"$PUB_CONN\"
        # RHEL 8.1 appends the word \"System\" in front of the connection, delete in case it exists
        nmcli con down \"System $PUB_CONN\"
        nmcli con delete \"System $PUB_CONN\"
        nmcli connection add ifname baremetal type bridge con-name baremetal bridge.stp no
        nmcli con add type bridge-slave ifname \"$PUB_CONN\" master baremetal
        pkill dhclient;dhclient baremetal
    "

---

### 2 - Preparing the OpenShift Container Platform installer

---

#### 2.0 - Using the stable-4.x version of the installation program

> $ export VERSION=stable-4.13
>

> $ export RELEASE_ARCH=<architecture>
>

> ```$ export RELEASE_IMAGE=$(curl -s https://mirror.openshift.com/pub/openshift-v4/$RELEASE_ARCH/clients/ocp/$VERSION/release.txt | grep 'Pull From: quay.io' | awk -F ' ' '{print $3}')```
>


#### 2.1 - Extracting the OpenShift Container Platform installer

> $ export cmd=openshift-baremetal-install
>

> $ export pullsecret_file=~/pull-secret.txt
>

> $ export extract_dir=$(pwd)
>

> $ curl -s https://mirror.openshift.com/pub/openshift-v4/clients/ocp/$VERSION/openshift-client-linux.tar.gz | tar zxvf - oc
>

> $ sudo cp oc /usr/local/bin
>

> ```$ oc adm release extract --registry-config "${pullsecret_file}" --command=$cmd --to "${extract_dir}" ${RELEASE_IMAGE}```
>

> $ sudo cp openshift-baremetal-install /usr/local/bin
>

#### 2.2 (optional, but recommended) Creating an RHCOS images cache

> $ sudo dnf install -y podman
>

> $ sudo firewall-cmd --add-port=8080/tcp --zone=public --permanent
>

> $ sudo firewall-cmd --reload
>

> $ mkdir /home/kni/rhcos_image_cache
>

> $ sudo semanage fcontext -a -t httpd_sys_content_t "/home/kni/rhcos_image_cache(/.*)?"
>

> $ sudo restorecon -Rv /home/kni/rhcos_image_cache/
>

> ```$ export RHCOS_QEMU_URI=$(/usr/local/bin/openshift-baremetal-install coreos print-stream-json | jq -r --arg ARCH "$(arch)" '.architectures[$ARCH].artifacts.qemu.formats["qcow2.gz"].disk.location')```
>

> $ export RHCOS_QEMU_NAME=${RHCOS_QEMU_URI##*/}
>

> ```$ export RHCOS_QEMU_UNCOMPRESSED_SHA256=$(/usr/local/bin/openshift-baremetal-install coreos print-stream-json | jq -r --arg ARCH "$(arch)" '.architectures[$ARCH].artifacts.qemu.formats["qcow2.gz"].disk["uncompressed-sha256"]')```
>

> ```$ curl -L ${RHCOS_QEMU_URI} -o /home/kni/rhcos_image_cache/${RHCOS_QEMU_NAME}```
>

> $ ls -Z /home/kni/rhcos_image_cache
>

> $ podman run -d --name rhcos_image_cache -v /home/kni/rhcos_image_cache:/var/www/html -p 8080:8080/tcp quay.io/centos7/httpd-24-centos7:latest
>

> $ export BAREMETAL_IP=$(ip addr show dev baremetal | awk '/inet /{print $2}' | cut -d"/" -f1)
>

> ```$ export BOOTSTRAP_OS_IMAGE="http://${BAREMETAL_IP}:8080/${RHCOS_QEMU_NAME}?sha256=${RHCOS_QEMU_UNCOMPRESSED_SHA256}"```
>

> $ echo "    bootstrapOSImage=${BOOTSTRAP_OS_IMAGE}"
>

-

__NOTE: Add the required configuration to the install-config.yaml file under platform.baremetal, replace <bootstrap_os_image> with the value of $BOOTSTRAP_OS_IMAGE__

    ...
    platform:
      baremetal:
        bootstrapOSImage: <bootstrap_os_image>
    ...

